<?php

namespace Netvlies\Bundle\CommandBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WinnerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('winner:is')
            ->setDescription('Who wins?')
            ->addArgument('who', InputArgument::REQUIRED|InputArgument::IS_ARRAY, 'Possible winners?')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'Yell the winner in uppercase')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $winners = $input->getArgument('who');
        for ($i = 0; $i < 500; ++$i) {
            shuffle($winners);
            $output->write(next($winners) ?: reset($winners));
            usleep(10000);
        }
        $winner = ($input->getOption('yell') === true) ? strtoupper(current($winners)) : current($winners);
        $output->writeln("\nWinner: ".$winner);
    }
}