<?php

namespace Netvlies\Bundle\TwigBundle\Twig\Extension;

class TextExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'text';
    }

    public function getFunctions()
    {
       return array(
           'paragraph' => new \Twig_Function_Method($this, 'paragraph', array(
               'is_safe' => array('html')
            )),
       );
    }

    public function paragraph($text, $options = array())
    {
        $css = (isset($options['class'])) ? ' class="'.$options['class'].'"' : '';

        $patterns = array("/(\r\n|\r)/", "/\n{2,}/");
        $replacements = array("\n", "</p><p$css>");

        # turn two and more newlines into paragraph
        $text = preg_replace($patterns, $replacements, $text);

        # turn single newline into <br/>
        $text = str_replace("\n", "\n<br />", $text);

        return '<p'.$css.'>'.$text.'</p>';
    }


}