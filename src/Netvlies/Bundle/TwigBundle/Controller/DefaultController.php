<?php

namespace Netvlies\Bundle\TwigBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/twig")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }

    /**
     * @Route("/hash")
     * @Template()
     */
    public function hashAction()
    {
        return array();
    }

    /**
     * @Route("/loop")
     * @Template()
     */
    public function loopAction()
    {
        $list = range(1, 20);
        return array('list' => $list);
    }

    /**
     * @Route("/paragraph")
     * @Template()
     */
    public function paragraphAction()
    {
        return array();
    }

    /**
     * @Route("/escape")
     * @Template()
     */
    public function escapeAction()
    {
        return array();
    }
}
